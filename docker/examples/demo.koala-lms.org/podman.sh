#!/usr/bin/env bash

declare -x POD="demo.koala-lms.org"
declare -x CONTAINER="demo"

podman stop -t=1 "${CONTAINER}"
podman rm "${CONTAINER}"

podman pod rm "${POD}"
podman pod create --name="${POD}" --share net -p 8080:8080

podman create --name="${CONTAINER}" \
              --pod="${POD}" \
              -e TIME_ZONE=Europe/Paris \
              -e FIXTURE=./fixtures/sample-fr.json \
              -e DEMO=1 \
              -e DEMONSTRATION_LOGIN=erik-orsenna \
              --add-host "${CONTAINER}":127.0.0.1 \
              --add-host "${POD}":127.0.0.1 \
              --hostname "${POD}" \
              registry.gitlab.com/koala-lms/lms:devel

podman start "${CONTAINER}"
